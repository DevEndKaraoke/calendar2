/* eslint-disable */
import * as functions from "firebase-functions";
import _ = require("lodash");

const store = require('firebase-admin');
store.initializeApp();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });



type appointment = {
     name: string,
     body: string,
     start: number,
     end: number
}

 export const dbtest = functions.https.onRequest( async (request, response) => {
   functions.logger.info("dbing", {structuredData: true});
   response.set('Access-Control-Allow-Origin', '*');
   response.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
   response.set('Access-Control-Allow-Headers', '*');
   await store.firestore().collection('test').add({'test': 2});
   response.send("Hello from the db");
 });

 /* 
    User
 */

 export const getAppointments = functions.https.onRequest( async (request, response) => {
     const slots = store.firestore().collection('appointments').onSnapshot((querySnapshot: any[]) => {
         var items = [];
         querySnapshot.forEach((doc) => {
            items.push(doc.data().name);
            console.log(doc);
            console.log(doc.data);
        });
        console.log('items: ' +  items);
    });
    console.log(slots);

    functions.logger.info("slots", {structuredData: true});
    response.send("slotting");
  });

 // Add appointment
 exports.addAppointment = functions.https.onCall(async (time: appointment, res) => {
    return new Promise((success, fail) => {
        if(time.start === undefined || time.end === undefined){
            functions.logger.log('Times missing');
            return fail({reason: 'mismatched times'});
        }
        if(time.start >=  time.end ){
            functions.logger.log('Times out of order');
            return fail({reason: 'time travel'});
        }
        functions.logger.log('adding appointment at ' + time.start + ' until ' + time.end);
        var unsubscribe = store.firestore().collection('appointments').onSnapshot((querySnapshot: any[]) => {
            var items = [];
            querySnapshot.forEach((doc) => {
                var newItem = {start: doc.data().start, end: doc.data().end};
               items.push(newItem)
           });
           for(var item in items){        
            if(checkOverlap(time, items[item])){
                functions.logger.log('Overlaps with existing booking');
                unsubscribe();
                return fail({reason: 'overlap'});
            }
           }
           store.firestore().collection('appointments').add(time);
           unsubscribe();
       });
    });

});
    


 /*
    Admin
 */

 /*
 * Currently will allow overlapping timeslots, since they can be of multiple length then
 * If that's not wanted, can easily copy over the overlap checking code
 */
 exports.addTimeslot = functions.https.onCall(async (time, res) => {
    if(time.start === undefined || time.end === undefined){
        return {status: 'fail'};
    }
    console.log('adding timeslot at ' + time.start + ' until ' + time.end);
    await store.firestore().collection('timeslots').add(time);
    return {status: 'success'};
});


export const getTimeslots = functions.https.onRequest( async (request, response) => {

    return new Promise((success, fail) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
    response.set('Access-Control-Allow-Headers', '*');
    console.log('getting timeslots');
    const slots = store.firestore().collection('timeslots').onSnapshot((querySnapshot: any[]) => {
        var items = [];
        console.log('snap: ' + querySnapshot);
        querySnapshot.forEach((doc) => {
            var newItem = {start: doc.data().start, end: doc.data().end};
            items.push(newItem)
       });
       console.log('items: ' +  items);
       console.log('item0: ' + items[0].start);
       //response.send(items);
       response.send({'data': items});
   });
   //console.log('slots2: ' + slots);
});

   //functions.logger.info("slots", {structuredData: true});
   //response.send("slotting");
});

exports.removeTimeslot = functions.https.onCall(async (time, res) => {
    return new Promise((success, fail) => {
        console.log('removing timeslot at ' + time.start + ' until ' + time.end);
        if(time.start === undefined || time.end === undefined){
            return fail({status: 'fail'});
        }
        const unsubscribe = store.firestore().collection('timeslots').onSnapshot((querySnapshot: any[]) => {
            querySnapshot.forEach((doc) => {
                if(doc.data().start === time.start && doc.data().end === time.end){
                    store.firestore().collection('timeslots').doc(doc.id).delete().then(() => {
                        console.log('timeslot removed');
                        unsubscribe();
                        return success({status: 'success'});                    
                    }).catch((error) => {
                        console.log('error in timeslot');
                        unsubscribe();
                        return fail({status: 'fail'});
                    });     
                }
            });
            return success({status: 'non existing timeslot'});        
        });
    });
});

 //Get past appointments 

 // Get upcoming appointments

 /*
 * Currently requires abs. stric info on the appointment, since timeslots can overlap and be
 * concurrent. This can easily be relaxed in the future so it's just with name or something
 */
 exports.removeAppointment = functions.https.onCall(async (time: appointment, res) => {
    return new Promise((success, fail) => {
        console.log('removing appointment at ' + time.start + ' until ' + time.end + ' booked by ' + time.name);
        if(time.start === undefined || time.end === undefined){
            return fail({status: 'fail'});
        }
        const unsubscribe = store.firestore().collection('appointments').onSnapshot((querySnapshot: any[]) => {
            querySnapshot.forEach((doc) => {
            if(compareObjects(doc.data(), time)){
                store.firestore().collection('appointments').doc(doc.id).delete().then(() => {
                    console.log('appointment removed');
                    unsubscribe();
                    return success({status: 'success'});                    
                }).catch((error) => {
                    console.log('error in appointments');
                    unsubscribe();
                    return fail({status: 'fail'});
                });     
            }
        });
        unsubscribe();
        return success({status: 'non existing timeslot'});        
        });
    });
});

 //Edit appointment


/*
    Internals
*/

export function checkOverlap(app1, app2){
    if(app1.start === undefined || app1.end === undefined || app2.start === undefined || app2.end === undefined){
        return true;
    }
    if(app1.start < app2.start ){
        var smallest = app1;
        var largest = app2;
    } else {
        var smallest = app2;
        var largest = app1;
    }
    if(smallest.end > largest.start){
        return true;
    }
    return false;
}

/*
* Since the usual methods don't work when fields are in a different order
*/
export function compareObjects(item1, item2){
    const sort1 = Object.keys(item1).sort();
    const sort2 = Object.keys(item2).sort();
    if(!(_.isEqual(sort1, sort2))){
        return false;
    } 
    const keys = Object.keys(item1);
    var checkbit = true; // fucking complains about paths so I have to do it this way
    keys.forEach((key, index) => {
        if(item1[key] !== item2[key]){
            checkbit = false;
        }
    });
    return checkbit;
}
