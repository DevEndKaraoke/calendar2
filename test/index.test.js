//const test = require('firebase-functions-test')();

const myFunctions = require('../src/index.ts');

const test = require('firebase-functions-test')({
    databaseURL: 'https://theend-calendar.firebaseio.com',
    storageBucket: 'theend-calendar.appspot.com',
    projectId: 'theend-calendar',
  }, '../../firebase.json.json');

const wrapped = test.wrap(myFunctions.checkOverlap);


return assert.equal(wrapped({item1: {start: 5, end: 7}, item2: {start: 6, end: 8}}) , true);



